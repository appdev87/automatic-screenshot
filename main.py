# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

from selenium import webdriver
from time import sleep
from datetime import datetime
import keyboard
import os
import platform

end_program = False
i = 0

driver = webdriver.Firefox()
path = ""


def set_folder_location():
    global path
    
        
    create_folder = input('Name your folder:\t')

    if platform.system() == "Windows":
        directory = 'C:\\Users\\louis\\OneDrive - LA TROBE UNIVERSITY\LaTrobe\\5th Semester\\CSE3PEX\\Course_Files'
        
    elif os.name == "posix":
        directory =  '/home/jlin/Documents/'
    else:
        raise SystemError('Unknown system detected')

    path = os.path.join(directory, create_folder)
    mode = 0o666
    os.mkdir(path, mode)


def create_pdf_from_screenshots():
    global path

    filename = input("What's the name of the pdf:\t")
    screenshot_list = []
    folder = os.listdir(path)
    print(folder)

    for screenshot in folder:
        screenshot_list.append(screenshot)

    # todo: complete the pdf section using either PIL or img2pdf libraries


def auto_mode():
    global i, path

    set_timer = input('What is the time-interval for taking screenshot [secs]?\t')
    print('Press any key to start and [q] to exit!\t')

    while 1:

        now = datetime.now()
        sleep(int(set_timer) - 1)
        current_time = now.strftime('%H%M%S')
        driver.get_screenshot_as_file(f"{path}/screenshot{current_time}.png")
        current_time = now.strftime('%H:%M:%S')
        print('Screenshot taken @', current_time)
        i += 1

        # Sleeps one seconds to listen for input
        sleep(1)
        if 0:
            break
        continue


def manual_mode():
    global i, path

    while True:
        user_input = input('Take a screenshot? [y] for yes and [n] to exit program!!!\t')

        if user_input == 'n':
            print("Scripting screenshot program ending...\n")
            break

        elif user_input == 'y':
            now = datetime.now()
            current_time = now.strftime('%H%M%S')
            driver.get_screenshot_as_file(f"{path}/screenshot{current_time}.png")
            current_time = now.strftime('%H:%M:%S')
            print('Screenshot taken @', current_time)

        else:
            continue


def start_program():
    global i

    user_input = input('Would your like to set your own URL? [y/n]\t')
    set_folder_location()

    if user_input == 'y':
        new_site_url = input('Please enter the URL to use to take screenshots...\n')
        driver.get(new_site_url)
    else:
        print('Loading page...')
        driver.get(
            'https://www.latrobe-didasko.com/learning-portal/mod/scorm/player.php?scoid=8565&cm=145390&currentorg=IT21_T1_org&display=popup')

        sleep(2)
        print('Please enter your credentials on the website and return to the program...')

    def ask_manual_or_automatic_mode():
        automatic_or_manual_mode = input('Would you like automatically timed screenshot or manual mode [a/m]\t')

        if automatic_or_manual_mode == 'a' or automatic_or_manual_mode == 'm':
            if automatic_or_manual_mode == 'a':
                auto_mode()

            elif automatic_or_manual_mode == 'm':
                manual_mode()

            else:
                print('Wrong input!!! Reset')
                start_program()
        else:
            print('Wrong input!!!')
            ask_manual_or_automatic_mode()

    return ask_manual_or_automatic_mode()


start_program()